package com.example.bts_todo_list_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView
import android.widget.ArrayAdapter
import android.util.Log

class MainActivity : AppCompatActivity() {
    private var TAG = "MainActivity"
    private lateinit var listView: ListView
    private lateinit var dataList: List<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //find list view
        listView = findViewById(R.id.list_view)
        //create list data
        var dataList = listOf("Snapchat", "Instagram", "Tiktok", "Facebook", "Youtube",
        "Camera", "Photos", "Gmail", "Email", "Settings", "Downloads", "Games", "Apps", "Contacts",
        "FaceTime", "Messenger", "Wallet", "Netflix", "Hulu", "HBOMax", "Clock", "Weather", "AppStore",
        "iTunes", "Calculator", "Maps", "Spotify", "Zoom", "Thank You", "End")
        dataList.forEach{
                item -> Log.d(TAG, item) // prints out each item in the list on a new log line
        }
        //hook list data up to our list using array adapter
        var arrayAdapter = ArrayAdapter(this, android.R.layout.simple_expandable_list_item_1, dataList)
        listView.adapter = arrayAdapter
    }
}